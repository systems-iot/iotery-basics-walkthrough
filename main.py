from iotery_embedded_python_sdk import Iotery
from time import time, sleep
from random import random

TEAM_ID="0cd0b3b6-d366-11e9-b548-d283610663ec"

iotery = Iotery()

d = iotery.getDeviceTokenBasic(data={"key": "my-vehicle",
                                     "serial": "my-vehicle", "secret": "secret", "teamUuid": TEAM_ID})

iotery.set_token(d["token"])

me = iotery.getMe()

timestep = 0
while timestep < 100:
    response = iotery.postData(deviceUuid=me["uuid"], data={
    "packets": [
        {
        "timestamp": int(time()),
        "deviceUuid": me["uuid"],
        "deviceTypeUuid": me["deviceType"]["uuid"],
        "data": {
            "speed": 10 + random()*10
        }
        }
    ]
    })
    
    for command_instance in response["unexecutedCommands"]["device"]:
        if command_instance["commandTypeUuid"] == "ebb9dbb8-d369-11e9-b548-d283610663ec":
            print("STOP VEHICLE!")
            iotery.setCommandInstanceAsExecuted(commandInstanceUuid=command_instance["uuid"], data={"timestamp":int(time())})
            print("Set command instance executed")
        
    print(response)

    
    sleep(1)